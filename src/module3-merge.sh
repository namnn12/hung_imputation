# this module reciprocally impute 1kgp3 & vn920
# +feature: reciprocally impute 1kgp3 & sg10k
# >>>>>>>>>>>>>>>>>>>>>>>> merge panel, reciprocal impute >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<< merge panel, reciprocal impute <<<<<<<<<<<<<<<<<<<<<<<<
mkdir -p ref/merge
# impute from vn920 to 1kgp, do not require pre-phase
INPUT="ref/vn920.chr20_forprephase.vcf.gz"
ARG="ref/1KGP3.chr20_forimpute.m3vcf.gz"
OUTPUT="ref/merge/"`basename $INPUT | sed 's/_.*/_impute.1KGP3/'` ; echo $OUTPUT
minimac4 --haps $INPUT --refHaps $ARG \
    --ChunkLengthMb 20 --ChunkOverlapMb 3 --allTypedSites \
    --prefix $OUTPUT --log --cpus 8 &
# impute from 1kgp to vn920, do not require pre-phase
INPUT="ref/1KGP3.chr20_forprephase.vcf.gz"
ARG="ref/vn920.chr20_forimpute.m3vcf.gz"
OUTPUT="ref/merge/"`basename $INPUT | sed 's/_.*/_impute.vn920/'` ; echo $OUTPUT
minimac4 --haps $INPUT --refHaps $ARG \
    --ChunkLengthMb 20 --ChunkOverlapMb 3 --allTypedSites \
    --prefix $OUTPUT --log --cpus 8 &
# merge *dose.vcf.gz [merge multi-allelic]; then remove them from OUTPUT
INPUT="ref/merge/vn920.chr20_impute.1KGP3.dose.vcf.gz"
bcftools index $INPUT &
INPUT2="ref/merge/1KGP3.chr20_impute.vn920.dose.vcf.gz"
bcftools index $INPUT2
OUTPUT="ref/merge-1KGP3-vn920.chr20_forprephase.vcf.gz"
bcftools merge --merge all $INPUT $INPUT2 -Ou |\
    bcftools view -M 2 -Oz -o $OUTPUT
[[ -f $OUTPUT.csi ]] || bcftools index $OUTPUT && echo "file was indexed"
# convert m3vcf
INPUT="ref/merge-1KGP3-vn920.chr20_forprephase.vcf.gz"
# bcftools index $INPUT
ARG=`basename $INPUT | grep -o 'chr[0-9X]*'`
OUTPUT="ref/`basename $INPUT | sed 's/_.*/_forimpute/'`" ; echo $OUTPUT
minimac3-omp --refHaps $INPUT --chr $ARG --processReference \
    --prefix $OUTPUT --log --cpus 8 &

# >>>>>>>>>>>>>>>>>>>>>>>> merge panel [sg10k + 1kgp3] >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<< merge panel [sg10k + 1kgp3] <<<<<<<<<<<<<<<<<<<<<<<<
# impute from sg10k to 1kgp, do not require pre-phase
INPUT="ref/sg10k.chr20_forprephase.vcf.gz"
ARG="ref/1KGP3.chr20_forimpute.m3vcf.gz"
OUTPUT="ref/merge/"`basename $INPUT | sed 's/_.*/_impute.1KGP3/'` ; echo $OUTPUT
minimac4 --haps $INPUT --refHaps $ARG \
    --ChunkLengthMb 20 --ChunkOverlapMb 3 --allTypedSites \
    --prefix $OUTPUT --log --cpus 5 &
# impute from 1kgp to sg10k, do not require pre-phase
INPUT="ref/1KGP3.chr20_forprephase.vcf.gz"
ARG="ref/sg10k.chr20_forimpute.m3vcf.gz"
OUTPUT="ref/merge/"`basename $INPUT | sed 's/_.*/_impute.sg10k/'` ; echo $OUTPUT
minimac4 --haps $INPUT --refHaps $ARG \
    --ChunkLengthMb 20 --ChunkOverlapMb 3 --allTypedSites \
    --prefix $OUTPUT --log --cpus 5 &
# merge *dose.vcf.gz [merge multi-allelic]; then remove them from OUTPUT
INPUT="ref/merge/sg10k.chr20_impute.1KGP3.dose.vcf.gz"
[[ -f $INPUT.csi ]] || bcftools index $INPUT &
INPUT2="ref/merge/1KGP3.chr20_impute.sg10k.dose.vcf.gz"
[[ -f $INPUT2.csi ]] || bcftools index $INPUT2 &
wait
OUTPUT="ref/merge-1KGP3-sg10k.chr20_forprephase.vcf.gz"
bcftools merge --merge all $INPUT $INPUT2 -Ou |\
    bcftools view -M 2 -Oz -o $OUTPUT
[[ -f $OUTPUT.csi ]] || bcftools index $OUTPUT
# convert m3vcf
INPUT="ref/merge-1KGP3-sg10k.chr20_forprephase.vcf.gz"
# bcftools index $INPUT
ARG=`basename $INPUT | grep -o 'chr[0-9X]*'`
OUTPUT="ref/`basename $INPUT | sed 's/_.*/_forimpute/'`"
minimac3-omp --refHaps $INPUT --chr $ARG --processReference \
    --prefix $OUTPUT --log --cpus 8 &

#### check missing
#### end check