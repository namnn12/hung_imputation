#!/bin/bash
# cd /mnt/area7/hung_dir/reference_data/DGV4VN
# mkdir -p /mnt/area7/hung_dir/reference_data/DGV4VN/dragen-output
OUTDIR="/mnt/area7/hung_dir/reference_data/DGV4VN/"
cd $OUTDIR
# dragen-output
INPUT="/mnt/area16/Population/Dragen_pipeline/testPop_1014.norm.filltags.pass.vcf.gz"
for i in {20,22}; do
  OUTPUT="dragen-output/dragen1014.chr${i}_lite.vcf.gz"
  echo $OUTPUT
  bcftools view -f "PASS" -r chr${i} $INPUT -Ou |\
    bcftools annotate -x ^FORMAT/GT --threads 4 -Oz -o $OUTPUT &
done
# gatk-output
INPUT="/mnt/area16/Population/GATK_pipeline/testPop_1016.gatk.norm.vqsr.vcf.gz"
for i in {20,22}; do
  OUTPUT="gatk-output/gatk1016.chr${i}_lite.vcf.gz"
  echo $OUTPUT
  bcftools view -f "PASS" -r chr${i} $INPUT -Ou |\
    bcftools annotate -x ^FORMAT/GT --threads 4 -Oz -o $OUTPUT &
done
# create symlink to working directory
WDIR="/mnt/area7/hung_dir/run_5"
cd $WDIR
ln -s /mnt/area7/hung_dir/reference_data/DGV4VN/gatk-output/gatk* ./data/
ln -s /mnt/area7/hung_dir/reference_data/DGV4VN/dragen-output/dragen* ./data/
# intersect samples
INPUT1="data/dragen1014.chr20_lite.vcf.gz"
INPUT2="data/gatk1016.chr20_lite.vcf.gz"
mkdir -p temp/data
LIST="temp/data/sample-list-dragen.txt"
bcftools query -l $INPUT1 > $LIST
OUTPUT="data/gatk1014.chr20_lite.vcf.gz"
bcftools view -S $LIST $INPUT2 -Oz -o $OUTPUT
# chr22
INPUT="data/gatk1016.chr22_lite.vcf.gz"
OUTPUT="data/gatk1014.chr22_lite.vcf.gz"
LIST="temp/data/sample-list-dragen.txt"
bcftools view -S $LIST $INPUT -Oz -o $OUTPUT
# >>>>>>>>>>>>>>>>>>>>>>>> consensus file for chr20 >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<< consensus file for chr20 <<<<<<<<<<<<<<<<<<<<<<<<
# intersect variants
INPUT1="data/dragen1014.chr20_lite.vcf.gz"
bcftools index $INPUT1
INPUT2="data/gatk1014.chr20_lite.vcf.gz"
bcftools index $INPUT2
bcftools isec -p temp/isec0/ -n~11 -c none $INPUT1 $INPUT2
# consensus file
INPUT1="temp/isec0/0000.vcf"
bgzip $INPUT1
bcftools index ${INPUT1}.gz &
INPUT2="temp/isec0/0001.vcf"
bgzip $INPUT2
bcftools index ${INPUT2}.gz
OUTPUT="temp/isec0/consensus.vcf.gz"
bcftools +isecGT ${INPUT1}.gz ${INPUT2}.gz -Oz -o $OUTPUT
bcftools index $OUTPUT
# >>>>>>>>>>>>>>>>>>>>>>>> consensus file for chr22 >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<< consensus file for chr22 <<<<<<<<<<<<<<<<<<<<<<<<
# intersect variants
INPUT1="data/dragen1014.chr22_lite.vcf.gz"
[[ -f $INPUT1.csi ]] || bcftools index $INPUT1
INPUT2="data/gatk1014.chr22_lite.vcf.gz"
[[ -f $INPUT2.csi ]] || bcftools index $INPUT2
bcftools isec -p temp/isec2/ -n~11 -c none -Oz $INPUT1 $INPUT2
# normalize gt order 0/. ./1
INPUT="temp/isec2/0000.vcf.gz"
OUTPUT="temp/isec2/0000_norm.vcf.gz"
bgzip -cd $INPUT | sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' | bgzip -c > $OUTPUT &
#
INPUT="temp/isec2/0001.vcf.gz"
OUTPUT="temp/isec2/0001_norm.vcf.gz"
bgzip -cd $INPUT | sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' | bgzip -c > $OUTPUT &
wait
# consensus file
INPUT1="temp/isec2/0001_norm.vcf.gz"
[[ -f $INPUT1.csi ]] || bcftools index $INPUT1
INPUT2="temp/isec2/0000_norm.vcf.gz"
[[ -f $INPUT2.csi ]] || bcftools index $INPUT2
OUTPUT="data/cnss1014.chr22_lite.vcf.gz"
bcftools +isecGT ${INPUT1} ${INPUT2} -Oz -o $OUTPUT
bcftools index $OUTPUT

# check the consensus file in docs/consensus.Rmd and move it to data for processing
mv $OUTPUT data/cnss1014.chr20_lite.vcf.gz
bcftools index data/cnss1014.chr20_lite.vcf.gz
# create symlink to input inside ./input
cd /mnt/area7/hung_dir/run_5
ln -s /mnt/area7/hung_dir/reference_data/APMRA/APMRA96_annotate.vcf.* ./input/
# create symlink to 1kgp3, sg10k
ln -s /mnt/area7/hung_dir/reference_data/1KGP_LC/1KGP3.chr20_QC1.vcf.gz* ./data/
ln -s /mnt/area7/hung_dir/reference_data/SG10K/hg38/SG10K.allchr.hg38_sort.vcf.gz* ./data/
#### upload data to hpc ####
# via sftp
# put *QC1* /dragennfs/area7/hung_dir/reference_data/1KGP_LC/
# consensus: /dragennfs/area7/hung_dir/run_5/data/cnss1014.chr20_lite.vcf.gz