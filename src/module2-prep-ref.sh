# ref required 2 files
## one phased vcf file for phasing with shapeit4
## one m3vcf files for imputation
### this module 

# >>>>>>>>>>>>>>>>>>>>>>>> SG10K >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<< SG10K <<<<<<<<<<<<<<<<<<<<<<<<
# create symlink
ln -s /mnt/area7/hung_dir/reference_data/SG10K/hg38/SG10K.allchr.hg38_sort.vcf.gz* data/
# extract chr20 and remove haploid due to liftover
INPUT="data/SG10K.allchr.hg38_sort.vcf.gz"
OUTPUT="temp/data/sg10k.chr20_QC.vcf.gz"
bcftools view -e 'GT="hap"' -r chr20 $INPUT -Oz -o $OUTPUT
# merge multi-alleleic, then rm them to get only bialllelic variant, rm AC<=2 for minimac3
# prep for prephase and convert m3vcf
INPUT=$OUTPUT
OUTPUT="ref/sg10k.chr20_forprephase.vcf.gz"
bcftools norm -m+any $INPUT -Ou |\
    bcftools view -v 'snps,indels' -M 2 -Ou |\
    bcftools view -i 'INFO/AC>2 | (INFO/AN-INFO/AC) >2' -Ou |\
    bcftools +fill-tags -Oz -o ${OUTPUT} -- -t MAF
# convert m3vcf
INPUT="ref/sg10k.chr20_forprephase.vcf.gz"
bcftools index $INPUT
ARG=`basename $INPUT | grep -o 'chr[0-9X]*'`
OUTPUT="ref/`basename $INPUT | sed 's/_.*/_forimpute/'`"
minimac3 --refHaps $INPUT --chr $ARG --processReference \
    --prefix $OUTPUT --log --cpus 8 &
# >>>>>>>>>>>>>>>>>>>>>>>> vn920 >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<< vn920 <<<<<<<<<<<<<<<<<<<<<<<<
# subset sample to create test and ref sample list using Rscript
ARG1="data/cnss1014.chr20_lite.vcf.gz"
ARG2="input/APMRA96_annotate.vcf.gz"
ARG3="temp/"
Rscript src/filter-sample-scr.R $ARG1 $ARG2 $ARG3
# subset based on list-ref-samples.txt
INPUT="data/cnss1014.chr20_lite.vcf.gz"
ARG="temp/list-ref-samples.txt"
OUTPUT="temp/data/vn920.chr20_lite.vcf.gz"
bcftools view -S $ARG $INPUT -Oz -o $OUTPUT
bcftools query -l $OUTPUT | wc -l
# process VN reference panel, in the same principle as 1KGP
# split multi-allelic variants, remove AC=0, and phase with shapeit4
INPUT=$OUTPUT
OUTPUT="temp/data/vn920.chr20_QC.vcf.gz"
bcftools norm -m- $INPUT -Ou |\
    bcftools view -i 'INFO/AC>0' -Oz -o $OUTPUT
bcftools index $OUTPUT
OUTPUT2="temp/data/vn920.chr20_phase.vcf.gz"
ARG1=`basename $OUTPUT | grep -o 'chr[0-9X]*'`
ARG2="/mnt/area7/hung_dir/reference_data/GeneticMap/GRCh38/shapeit_geneticmap_${ARG1}_hg38.txt"
shapeit4 --input $OUTPUT --sequencing --region $ARG1 --map $ARG2 \
    --output $OUTPUT2 --log ${OUTPUT2%.vcf*}.log --thread 8 &&
bcftools index $OUTPUT2
# remove multi-allelic variants and fill-tags MAF
INPUT="temp/data/vn920.chr20_phase.vcf.gz"
OUTPUT="ref/vn920.chr20_forprephase.vcf.gz"
bcftools norm -m+any $INPUT -Ou |\
  bcftools view -v 'snps,indels' -M 2 -Ou |\
  bcftools +fill-tags -Oz -o $OUTPUT -- -t MAF
bcftools index $OUTPUT
# convert m3vcf
INPUT="ref/vn920.chr20_forprephase.vcf.gz"
bcftools index $INPUT
ARG=`basename $INPUT | grep -o 'chr[0-9X]*'`
OUTPUT="ref/`basename $INPUT | sed 's/_.*/_forimpute/'`" ; echo $OUTPUT
minimac3-omp --refHaps $INPUT --chr $ARG --processReference \
    --prefix $OUTPUT --log --cpus 8 &
#

# >>>>>>>>>>>>>>>>>>>>>>>> 1kgp3 >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<< 1kgp3 <<<<<<<<<<<<<<<<<<<<<<<<
# symlink data
ln -s /mnt/area7/hung_dir/reference_data/1KGP_LC/1KGP3.chr20_QC1.vcf.gz* data/
# remove multi-alleleic variants, remove singleton and doubleton variants
INPUT="data/1KGP3.chr20_QC1.vcf.gz"
OUTPUT="ref/1KGP3.chr20_forprephase.vcf.gz"
bcftools norm -m+any $INPUT -Ou |\
    bcftools view -v 'snps,indels' -M 2 -Ou |\
    bcftools view -i 'INFO/AC>2 | (INFO/AN-INFO/AC) >2' -Ou |\
    bcftools +fill-tags -Oz -o ${OUTPUT} -- -t MAF
# convert m3vcf with openMP
INPUT="ref/1KGP3.chr20_forprephase.vcf.gz"
bcftools index $INPUT
ARG=`basename $INPUT | grep -o 'chr[0-9X]*'`
OUTPUT="ref/`basename $INPUT | sed 's/_.*/_forimpute/'`" ; echo $OUTPUT
minimac3-omp --refHaps $INPUT --chr $ARG --processReference \
    --prefix $OUTPUT --log --cpus 8 &
# >>>>>>>>>>>>>>>>>>>>>>>>  >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<<  <<<<<<<<<<<<<<<<<<<<<<<<