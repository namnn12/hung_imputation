#!/usr/bin/env bash

# run docker at working dir in local
docker run --name imputation \
    -v ${PWD}:/mnt/ \
    -v /Users/hung/Tools/Library/:/mnt/reference/ \
    -it thehung92phuyen/biotools:v4.0 bash

# run docker at working dir in hpc
docker run --name imputation \
    -v /dragennfs/area7/:/mnt/area7/ \
    -v /dragennfs/area16/:/mnt/area16/ \
    -it registry.vinbdi.org/imputation:v5.2 bash

# use new container to debug merging step
docker pull registry.vinbdi.org/biotools:v4.1
# remove the old container
docker stop imputation
docker rm imputation
# run docker with the new images in hpc
docker run --name imputation \
    -v /dragennfs/area7/:/mnt/area7/ \
    -v /dragennfs/area16/:/mnt/area16/ \
    -it registry.vinbdi.org/biotools:v4.5 bash