#!/usr/bin/env bash
# >>>>>>>>>>>>>>>>>>>>>>>> consensus file for chr22 >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<< consensus file for chr22 <<<<<<<<<<<<<<<<<<<<<<<<
mkdir -p temp/consensus
OUTDIR="temp/consensus/"
# intersect samples and sort them in alphabetical order
INPUT1="data/gatk1016.chr22_lite.vcf.gz"
bcftools query -l $INPUT1 | sort > ${OUTDIR}list_gatk-samples.txt
INPUT2="data/dragen1014.chr22_lite.vcf.gz"
bcftools query -l $INPUT2 | sort > ${OUTDIR}list_dragen-samples.txt
comm -12 ${OUTDIR}list_gatk-samples.txt ${OUTDIR}list_dragen-samples.txt > ${OUTDIR}list_common-samples.txt
# extract lite files from input with samples in order
INPUT1="data/gatk1016.chr22_lite.vcf.gz"
OUTPUT1="${OUTDIR}"`basename $INPUT1 | sed 's/.vcf/_lite.vcf/'` ; echo $OUTPUT1
bcftools view -S ${OUTDIR}list_common-samples.txt $INPUT1 |\
    bcftools annotate -x INFO,^FORMAT/GT -Oz -o $OUTPUT1 &
#
INPUT2="data/dragen1014.chr22_lite.vcf.gz"
OUTPUT2="${OUTDIR}"`basename $INPUT2 | sed 's/.vcf/_lite.vcf/'` ; echo $OUTPUT2
bcftools view -S ${OUTDIR}list_common-samples.txt $INPUT2 |\
    bcftools annotate -x INFO,^FORMAT/GT -Oz -o $OUTPUT2 &
wait
# intersect variants
INPUT1=$OUTPUT1
[[ -f $INPUT1.csi ]] || bcftools index $INPUT1 &
INPUT2=$OUTPUT2
[[ -f $INPUT2.csi ]] || bcftools index $INPUT2 &
wait
bcftools isec -p ${OUTDIR} -n~11 -c none $INPUT1 $INPUT2
# normalize gt order 0/. ./1
INPUT="${OUTDIR}0000.vcf"
OUTPUT="${OUTDIR}0000_norm.vcf.gz"
# bgzip -cd $INPUT | sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' | bgzip -c > $OUTPUT &
sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' $INPUT | bgzip -c > $OUTPUT &
#
INPUT="${OUTDIR}0001.vcf"
OUTPUT="${OUTDIR}0001_norm.vcf.gz"
# bgzip -cd $INPUT | sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' | bgzip -c > $OUTPUT &
sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' $INPUT | bgzip -c > $OUTPUT &
wait
# consensus file
INPUT1="${OUTDIR}0000_norm.vcf.gz"
[[ -f $INPUT1.csi ]] || bcftools index $INPUT1 &
INPUT2="${OUTDIR}0001_norm.vcf.gz"
[[ -f $INPUT2.csi ]] || bcftools index $INPUT2 &
wait
OUTPUT="${OUTDIR}cnss1014.chr22_lite.vcf.gz"
bcftools +isecGT ${INPUT1} ${INPUT2} -Oz -o $OUTPUT
bcftools index $OUTPUT