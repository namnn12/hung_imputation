#!/usr/bin/env bash
i=$1
LOG=$2
    INPUT1="data/testPop_1026.gatk.norm.passVQSR.rm13Sample.vcf.gz"
    OUTPUT1="temp/gatk1013.chr${i}_lite.vcf.gz"
    bcftools view -r chr${i} $INPUT1 |\
    bcftools annotate -x ^FORMAT/GT -Oz -o $OUTPUT1 &
    #
    INPUT2="data/testPop_1013.dragen.norm.pass.vcf.gz"
    OUTPUT2="temp/dragen1013.chr${i}_lite.vcf.gz"
    bcftools view -r chr${i} $INPUT2 |\
    bcftools annotate -x ^FORMAT/GT -Oz -o $OUTPUT2 &
    wait
    #
    # intersect variants i=22
    INPUT1=$OUTPUT1
    echo $INPUT1 >> ${LOG}
    [[ -f $INPUT1.csi ]] || bcftools index $INPUT1 &
    INPUT2=$OUTPUT2
    echo $INPUT2 >> ${LOG}
    [[ -f $INPUT2.csi ]] || bcftools index $INPUT2 &
    wait
    IDIR="temp/isec_chr${i}"
    bcftools isec -p $IDIR -n~11 -c none $INPUT1 $INPUT2
    #
    # normalize gt order 0/. ./1
    INPUT="${IDIR}/0000.vcf"
    OUTPUT="${IDIR}/0000_norm.vcf.gz"
    # bgzip -cd $INPUT | sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' | bgzip -c > $OUTPUT &
    sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' $INPUT | bgzip -c > $OUTPUT &
    #
    INPUT="${IDIR}/0001.vcf"
    OUTPUT="${IDIR}/0001_norm.vcf.gz"
    # bgzip -cd $INPUT | sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' | bgzip -c > $OUTPUT &
    sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' $INPUT | bgzip -c > $OUTPUT &
    wait
    echo "complete $IDIR" >> ${LOG}
    # rm ${IDIR}/000*.vcf
    #
    # consensus file
    INPUT1="${IDIR}/0000_norm.vcf.gz"
    [[ -f $INPUT1.csi ]] || bcftools index $INPUT1 &
    INPUT2="${IDIR}/0001_norm.vcf.gz"
    [[ -f $INPUT2.csi ]] || bcftools index $INPUT2 &
    wait
    OUTPUT="output/cnss1013.chr${i}_lite.vcf.gz"
    bcftools +isecGT ${INPUT1} ${INPUT2} |\
        bcftools view -Oz -o $OUTPUT
    bcftools index -t $OUTPUT
echo "complete $OUTPUT" >> ${LOG}