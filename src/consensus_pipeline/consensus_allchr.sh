#!/usr/bin/env bash
docker run --name imputation \
    -v /dragennfs/area7/:/mnt/area7/ \
    -v /dragennfs/area16/:/mnt/area16/ \
    -it registry.vinbdi.org/biotools:v4.3 bash
#### input path ####
# gatk
INPUT1="/mnt/area16/Population/final/GATK_finalset/testPop_1026.gatk.norm.passVQSR.rm13Sample.vcf.gz"
zless $INPUT1
# drgn
INPUT2="/mnt/area16/Population/final/Dragen_finalset/testPop_1013.dragen.norm.pass.vcf.gz"
zless $INPUT2
#### working dir ####
cd /mnt/area16/Population/consensus_pipelines/run_101
mkdir data src output
ln -s ${INPUT1}* data/
ln -s ${INPUT2}* data/
#### check samples order ####
mkdir -p temp
OUTDIR="temp/"
INPUT1="data/testPop_1026.gatk.norm.passVQSR.rm13Sample.vcf.gz"
bcftools query -l $INPUT1 > ${OUTDIR}list_gatk-samples.txt
INPUT2="data/testPop_1013.dragen.norm.pass.vcf.gz"
bcftools query -l $INPUT2 > ${OUTDIR}list_dragen-samples.txt
# check if they are in the same order
cmp ${OUTDIR}list_gatk-samples.txt ${OUTDIR}list_dragen-samples.txt
# comm -12 ${OUTDIR}list_gatk-samples.txt ${OUTDIR}list_dragen-samples.txt > ${OUTDIR}list_common-samples.txt
# extract lite files from input with samples in order
for i in {{1..21},X}; do
    echo ${i}
    INPUT1="data/testPop_1026.gatk.norm.passVQSR.rm13Sample.vcf.gz"
    OUTPUT1="temp/gatk1013.chr${i}_lite.vcf.gz"
    bcftools view -r chr${i} $INPUT1 |\
    bcftools annotate -x ^FORMAT/GT -Oz -o $OUTPUT1 &
    #
    INPUT2="data/testPop_1013.dragen.norm.pass.vcf.gz"
    OUTPUT2="temp/dragen1013.chr${i}_lite.vcf.gz"
    bcftools view -r chr${i} $INPUT2 |\
    bcftools annotate -x ^FORMAT/GT -Oz -o $OUTPUT2 &
    wait
    #
    # intersect variants i=22
    INPUT1=$OUTPUT1
    [[ -f $INPUT1.csi ]] || bcftools index $INPUT1 &
    INPUT2=$OUTPUT2
    [[ -f $INPUT2.csi ]] || bcftools index $INPUT2 &
    wait
    IDIR="temp/isec_chr${i}"
    bcftools isec -p $IDIR -n~11 -c none $INPUT1 $INPUT2
    #
    # normalize gt order 0/. ./1
    INPUT="${IDIR}/0000.vcf"
    OUTPUT="${IDIR}/0000_norm.vcf.gz"
    # bgzip -cd $INPUT | sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' | bgzip -c > $OUTPUT &
    sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' $INPUT | bgzip -c > $OUTPUT &
    #
    INPUT="${IDIR}/0001.vcf"
    OUTPUT="${IDIR}/0001_norm.vcf.gz"
    # bgzip -cd $INPUT | sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' | bgzip -c > $OUTPUT &
    sed '/^chr/s/1\/\./\.\/1/g ; /^chr/s/\.\/0/0\/\./g' $INPUT | bgzip -c > $OUTPUT &
    wait
    rm ${IDIR}/000*.vcf
    #
    # consensus file
    INPUT1="${IDIR}/0000_norm.vcf.gz"
    [[ -f $INPUT1.csi ]] || bcftools index $INPUT1 &
    INPUT2="${IDIR}/0001_norm.vcf.gz"
    [[ -f $INPUT2.csi ]] || bcftools index $INPUT2 &
    wait
    OUTPUT="output/cnss1013.chr${i}_lite.vcf.gz"
    bcftools +isecGT ${INPUT1} ${INPUT2} |\
        bcftools view -Oz -o $OUTPUT
    bcftools index -t $OUTPUT
done

parallel --jobs 8 bash src/consensus_loop.sh ::: {{1..21},X}

