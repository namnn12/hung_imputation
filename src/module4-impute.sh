# >>>>>>>>>>>>>>>>>>>>>>>> prep input >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<< prep input <<<<<<<<<<<<<<<<<<<<<<<<
INPUT="input/APMRA96_annotate.vcf.gz"
mkdir -p temp/input
FASTA="/mnt/area7/hung_dir/reference_data/GRCh38/Homo_sapiens_assembly38.fasta"
OUTPUT="temp/input/apmra96.chr20_QC.vcf.gz"
# ref correct and remove duplicate position
bcftools norm -f $FASTA -c ws -r chr20 $INPUT -Ou |\
    bcftools norm -m+any -Ou |\
    bcftools view -M 2 -Oz -o $OUTPUT
# subset based on sample list and rename them to sequencing convention
INPUT=$OUTPUT
ARG1="temp/list-input-samples.txt"
ARG2="temp/samples-rename.txt"
OUTPUT="input/apmra94.chr20_prepimpute.vcf.gz"
bcftools view -S $ARG1 $INPUT -Ov |\
  bcftools reheader -s $ARG2 | bgzip -c > $OUTPUT
bcftools index $OUTPUT
# >>>>>>>>>>>>>>>>>>>>>>>> phase and impute >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<< phase and impute <<<<<<<<<<<<<<<<<<<<<<<<
# source the function 
# _imputefunc $INPUT $REF
source src/_imputefunc.sh
# impute with vn920
INPUT="input/apmra94.chr20_prepimpute.vcf.gz"
REF="ref/vn920.chr20_forprephase.vcf.gz"
_imputefunc $INPUT $REF &
# impute with 1KGP3
INPUT="input/apmra94.chr20_prepimpute.vcf.gz"
REF="ref/1KGP3.chr20_forprephase.vcf.gz"
_imputefunc $INPUT $REF &
# impute with sg10k
INPUT="input/apmra94.chr20_prepimpute.vcf.gz"
REF="ref/sg10k.chr20_forprephase.vcf.gz"
_imputefunc $INPUT $REF &
# impute with merge-1KGP3-vn920
INPUT="input/apmra94.chr20_prepimpute.vcf.gz"
REF="ref/merge-1KGP3-vn920.chr20_forprephase.vcf.gz"
_imputefunc $INPUT $REF &
# impute with merge-1KGP3-sg10k
INPUT="input/apmra94.chr20_prepimpute.vcf.gz"
REF="ref/merge-1KGP3-sg10k.chr20_forprephase.vcf.gz"
_imputefunc $INPUT $REF &