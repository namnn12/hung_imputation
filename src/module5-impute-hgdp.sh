#!/bin/bash
# create symlink of HGDP data to input dir
ln -s /mnt/area7/hung_dir/reference_data/HGDP/HGDP.b38_aligned.vcf.gz* input/
# >>>>>>>>>>>>>>>>>>>>>>>> process input array >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<< process input array <<<<<<<<<<<<<<<<<<<<<<<<
# prepare the array data in chr20
# ref correct and remove duplicate position
INPUT="input/HGDP.b38_aligned.vcf.gz"
mkdir -p temp/input
FASTA="/mnt/area7/hung_dir/reference_data/GRCh38/Homo_sapiens_assembly38.fasta"
OUTPUT="temp/input/HGDP.chr20_QC.vcf.gz"
bcftools norm -f $FASTA -c ws -r chr20 $INPUT -Ou |\
    bcftools norm -m+any -Ou |\
    bcftools view -M 2 -Oz -o $OUTPUT
[[ ! -f $OUTPUT.csi ]] && bcftools index $OUTPUT

# >>>>>>>>>>>>>>>>>>>>>>>> simulate kinh94 to hgdp_array from sequencing data >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<< simulate kinh94 to hgdp_array from sequencing data <<<<<<<<<<<<<<<<<<<<<<<<
# create hgdp type array [Illumina650k] from vn94 sequencing data [with split multi-allelic]
INPUT="data/cnss1014.chr20_lite.vcf.gz"
ARG1="temp/samples-rename.txt"
ARG2="temp/list-kinh94.txt"
awk '{print $2}' $ARG1 > $ARG2
OUTPUT="input/Kinh94.chr20_QC.vcf.gz"
bcftools view -S $ARG2 $INPUT -Oz -o $OUTPUT
# subset with bcftools isec based on chrom:pos:ref:alt
INPUT="input/Kinh94.chr20_QC.vcf.gz"
[[ ! -f $INPUT.csi ]] && bcftools index $INPUT
ARG="temp/input/HGDP.chr20_QC.vcf.gz"
[[ ! -f $ARG.csi ]] && bcftools index $ARG
OUTDIR="temp/isec1"
bcftools isec -c none -n~11 $INPUT $ARG -p $OUTDIR -Oz -w 1
OUTPUT="temp/isec1/0000.vcf.gz"
# merge with HGDP file assuming missing value is 0/0
INPUT1="temp/isec1/0000.vcf.gz"
INPUT2="temp/input/HGDP.chr20_QC.vcf.gz"
OUTPUT="input/HGDP-vn94.chr20_QC.vcf.gz"
bcftools merge --missing-to-ref -m none $INPUT1 $INPUT2 -Oz -o $OUTPUT
[[ -f  ${OUTPUT}.csi ]] || bcftools index $OUTPUT
# masked snp in vcf file using snp.list
INPUT="input/HGDP-vn94.chr20_QC.vcf.gz"
ARG="temp/masked.snps.list"
OUTPUT="input/HGDP-vn94.chr20_prepimpute.vcf.gz"
bcftools view -T ^$ARG $INPUT -Oz -o $OUTPUT
[[ -f  ${OUTPUT}.csi ]] || bcftools index $OUTPUT

# >>>>>>>>>>>>>>>>>>>>>>>> impute >>>>>>>>>>>>>>>>>>>>>>>>
# <<<<<<<<<<<<<<<<<<<<<<<< impute <<<<<<<<<<<<<<<<<<<<<<<<
# source function
. src/_imputefunc.sh
# vn920
INPUT="input/HGDP-vn94.chr20_prepimpute.vcf.gz"
REF="ref/vn920.chr20_forprephase.vcf.gz"
_imputefunc $INPUT $REF &
# 1KGP3
INPUT="input/HGDP-vn94.chr20_prepimpute.vcf.gz"
REF="ref/1KGP3.chr20_forprephase.vcf.gz"
_imputefunc $INPUT $REF &
# impute with sg10k
INPUT="input/HGDP-vn94.chr20_prepimpute.vcf.gz"
REF="ref/sg10k.chr20_forprephase.vcf.gz"
_imputefunc $INPUT $REF &
# impute with merge-1KGP3-vn920
INPUT="input/HGDP-vn94.chr20_prepimpute.vcf.gz"
REF="ref/merge-1KGP3-vn920.chr20_forprephase.vcf.gz"
#_customfunc -n $INPUT $REF
_imputefunc $INPUT $REF &
# impute with merge-1KGP3-sg10k
INPUT="input/HGDP-vn94.chr20_prepimpute.vcf.gz"
REF="ref/merge-1KGP3-sg10k.chr20_forprephase.vcf.gz"
# _customfunc $INPUT $REF
_imputefunc $INPUT $REF &