#!/usr/bin/env bash

# main command
mkdir -p temp/filter
INPUT="data/gatk1014.chr22_lite.vcf.gz"
[[ -f $INPUT.tbi ]] || bcftools index -t $INPUT
OUTPUT="temp/filter/gatk1014.chr22_hardfilter.vcf.gz"
gatk VariantFiltration \
    -V $INPUT \
    -filter "QD < 2.0" --filter-name "QD2" \
    -filter "QUAL < 30.0" --filter-name "QUAL30" \
    -filter "SOR > 3.0" --filter-name "SOR3" \
    -filter "FS > 60.0" --filter-name "FS60" \
    -filter "MQRankSum < -12.5" --filter-name "MQRankSum-12.5" \
    -filter "ReadPosRankSum < -8.0" --filter-name "ReadPosRankSum-8" \
    -O $OUTPUT
# 
bcftools view -i 'FILTER="PASS"' -GH $INPUT | wc -l
bcftools view -i 'FILTER="PASS"' -GH $OUTPUT | wc -l

# >>>>>>>>>>>>>>>>>>>>>>>> original >>>>>>>>>>>>>>>>>>>>>>>>
gatk VariantFiltration \
    -V $INPUT \
    -filter "QD < 2.0" --filter-name "QD2" \
    -filter "QUAL < 30.0" --filter-name "QUAL30" \
    -filter "SOR > 3.0" --filter-name "SOR3" \
    -filter "FS > 60.0" --filter-name "FS60" \
    -filter "MQ < 40.0" --filter-name "MQ40" \
    -filter "MQRankSum < -12.5" --filter-name "MQRankSum-12.5" \
    -filter "ReadPosRankSum < -8.0" --filter-name "ReadPosRankSum-8" \
    -O $OUTPUT
# <<<<<<<<<<<<<<<<<<<<<<<< original <<<<<<<<<<<<<<<<<<<<<<<<

# subset

INPUT=""

bcftools view -S samplelist -r chr22 -

bcftools annotate -x ^FORMAT/GT
#