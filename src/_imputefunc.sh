#!/usr/bin/env bash
#
_imputefunc() {
    INPUT=$1
    # INPUT=""input/apmra94.chr20_prepimpute.vcf.gz""
    ARG1=`basename $INPUT | grep -o 'chr[0-9X]*'`
    ARG2="/mnt/area7/hung_dir/reference_data/GeneticMap/GRCh38/shapeit_geneticmap_${ARG1}_hg38.txt"
    ARG3=$2
    # ARG3="ref/1KGP3.chr20_forprephase.vcf.gz"
    OUTPUT="temp/input/`basename $INPUT | sed 's/_.*/_phase./'``basename $ARG3 | sed 's/.chr.*//'`.vcf.gz"; echo $OUTPUT
    shapeit4 --input $INPUT --region $ARG1 --map $ARG2 \
        --reference $ARG3 --output $OUTPUT --log ${OUTPUT%.vcf*}.log --thread 8
    # impute with minimac
    INPUT=$OUTPUT
    ARG1=`basename $INPUT | sed 's/.*phase.\(.*\).vcf.*/\1/'`
    ARG2=`ls ref/${ARG1}*"m3vcf.gz"`
    OUTPUT="output/"`basename $INPUT | sed 's/phase/impute/; s/.vcf.gz//'` ; echo $OUTPUT
    minimac4 --haps $INPUT --refHaps $ARG2 \
        --ChunkLengthMb 20 --ChunkOverlapMb 3 --allTypedSites \
        --prefix $OUTPUT --log --cpus 8
}
#
_customfunc() {
while getopts ":ni:r:" opt; do
  case ${opt} in
    n ) local dry_run=1 && echo 'only echo filenames'
        ;;
    i ) echo "input is $OPTARG"
        INPUT=$OPTARG
        ;;
    r ) echo "ref is $OPTARG"
        ARG3=$OPTARG
        ;;
    \? ) echo "Usage: cmd [-n] [-i input] [-r ref]"
      ;;
  esac
done
# shift the parameter in optarg
shift $((OPTIND -1))
# main command after parsing opts
if [ -z ${dry_run+x} ];
then
    INPUT=$1
    # INPUT=""input/apmra94.chr20_prepimpute.vcf.gz""
    ARG1=`basename $INPUT | grep -o 'chr[0-9X]*'`
    ARG2="/mnt/area7/hung_dir/reference_data/GeneticMap/GRCh38/shapeit_geneticmap_${ARG1}_hg38.txt"
    ARG3=$2
    # ARG3="ref/1KGP3.chr20_forprephase.vcf.gz"
    OUTPUT="temp/input/`basename $INPUT | sed 's/_.*/_phase./'``basename $ARG3 | sed 's/.chr.*//'`.vcf.gz"; echo $OUTPUT
    shapeit4 --input $INPUT --region $ARG1 --map $ARG2 \
        --reference $ARG3 --output $OUTPUT --log ${OUTPUT%.vcf*}.log --thread 8
    # impute with minimac
    INPUT=$OUTPUT
    ARG1=`basename $INPUT | sed 's/.*phase.\(.*\).vcf.*/\1/'`
    ARG2=`ls ref/${ARG1}*"m3vcf.gz"`
    OUTPUT="output/"`basename $INPUT | sed 's/phase/impute/; s/.vcf.gz//'` ; echo $OUTPUT
    minimac4 --haps $INPUT --refHaps $ARG2 \
        --ChunkLengthMb 20 --ChunkOverlapMb 3 --allTypedSites \
        --prefix $OUTPUT --log --cpus 8
else
    INPUT=$1
    echo "INPUT is $INPUT"
    # INPUT=""input/apmra94.chr20_prepimpute.vcf.gz""
    ARG1=`basename $INPUT | grep -o 'chr[0-9X]*'`
    ARG2="/mnt/area7/hung_dir/reference_data/GeneticMap/GRCh38/shapeit_geneticmap_${ARG1}_hg38.txt"
    ARG3=$2
    echo "REF is $ARG3"
    # ARG3="ref/1KGP3.chr20_forprephase.vcf.gz"
    OUTPUT="temp/input/`basename $INPUT | sed 's/_.*/_phase./'``basename $ARG3 | sed 's/.chr.*//'`.vcf.gz"
    echo "prephase output is $OUTPUT"
    # shapeit4 --input $INPUT --region $ARG1 --map $ARG2 \
    #     --reference $ARG3 --output $OUTPUT --log ${OUTPUT%.vcf*}.log --thread 8
    # impute with minimac
    INPUT=$OUTPUT
    ARG1=`basename $INPUT | sed 's/.*phase.\(.*\).vcf.*/\1/'`
    ARG2=`ls ref/${ARG1}*"m3vcf.gz"`
    echo "m3vcf path is $ARG2"
    OUTPUT="output/"`basename $INPUT | sed 's/phase/impute/; s/.vcf.gz//'`
    echo "impute output is $OUTPUT"
    # minimac4 --haps $INPUT --refHaps $ARG2 \
    #     --ChunkLengthMb 20 --ChunkOverlapMb 3 --allTypedSites \
    #     --prefix $OUTPUT --log --cpus 8
fi
}